
/**

  Understanding npm package Q.

  Used in: src/app/database.js

  Q.denodeify(function):
    Creates a promise-returning function from a Node.js-style function,
    optionally binding it with the given variadic arguments.

*/

var fs = require('fs');
var Q = require('q');

//Wrapping the function in a promise function.
var readFile = Q.denodeify(fs.readFile);

readFile('package.json', 'utf-8').then(function(data){
  console.log('ReadFile >> ' + data);
}).catch(function(err) {
  console.log('ERR >> ' + err);
});

//Creating a promises Q.defer()
var readFile2 = function(path, enc) {

  var deferred = Q.defer();

  fs.readFile(path, enc, function(err, data) {

    if(err) {deferred.reject(err);}

    else {deferred.resolve(data);}

  });

  return deferred.promise;

}

readFile2('package.json', 'utf-8').then(function(data){
  console.log('ReadFile2 >> ' + data);
}).catch(function(err) {
  console.log('ERR >> ' + err);
});
